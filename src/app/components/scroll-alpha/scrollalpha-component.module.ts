import { ScrollAlphaComponent } from './scroll-alpha.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

@NgModule({
	declarations: [
		ScrollAlphaComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		RouterModule,
		
		
	],
	exports: [
		ScrollAlphaComponent
	]
})
export class ScrollalphaComponentsModule {}
