import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-pass-item',
  templateUrl: './pass-item.component.html',
  styleUrls: ['./pass-item.component.scss'],
})
export class PassItemComponent implements OnInit {

  @Input() id;
  @Input() image = null;
  @Input() heading = null;
  @Input() text = null;
  @Input() small = null;
  @Input() qrcode = '123465';
  @Output('fireOptions') fireOptions: EventEmitter<any> = new EventEmitter<any>();


  constructor() { }

  ngOnInit() {}

  option($event){
    this.fireOptions.emit($event);
  }
  
}
