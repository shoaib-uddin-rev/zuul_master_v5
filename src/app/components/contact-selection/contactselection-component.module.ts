import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { ContactSelectionComponent } from './contact-selection.component';
import { EmptyviewComponentsModule } from '../empty-view/emptyview-component.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
	declarations: [
		ContactSelectionComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		RouterModule,
		FormsModule,
		ReactiveFormsModule,
		EmptyviewComponentsModule
		
	],
	exports: [
		ContactSelectionComponent
	]
})
export class ContactselectionComponentsModule {}
