import { EmptyViewComponent } from './empty-view.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
@NgModule({
	declarations: [
		EmptyViewComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		RouterModule,
		
	],
	exports: [
		EmptyViewComponent
	]
})
export class EmptyviewComponentsModule {}
