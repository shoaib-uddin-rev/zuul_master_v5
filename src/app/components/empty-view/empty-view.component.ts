import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-empty-view',
  templateUrl: './empty-view.component.html',
  styleUrls: ['./empty-view.component.scss'],
})
export class EmptyViewComponent implements OnInit {

  @Input() text: string = 'No Records Found';
  @Input() icon: string = 'alert';
  @Input() image: string = '';

  constructor() { }

  ngOnInit() {}

}
