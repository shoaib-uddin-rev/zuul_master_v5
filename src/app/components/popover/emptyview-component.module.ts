import { PopoverComponent } from './popover.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
@NgModule({
	declarations: [
		PopoverComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		RouterModule,
		
	],
	exports: [
		PopoverComponent
	]
})
export class PopoverComponentsModule {}
