import { Component, OnInit, Input, Injector } from '@angular/core';
import { PopoverController, NavParams } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';
import { SqliteService } from 'src/app/services/sqlite.service';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.component.html',
  styleUrls: ['./popover.component.scss'],
})
export class PopoverComponent implements OnInit {

  @Input() pid;
  @Input() flag = 'C';
  @Input() delete_label = 'Delete';

  user: any;
  sw_users: any = [];
  // two flags for family member dropdown
  can_manage_family: boolean = false;
  can_send_passes: boolean = false;
  can_request_passes: boolean = false;
  allow_parental_control: boolean = false;
  lock_head_of_family: boolean = false;

  constructor(public navParams: NavParams, public userService: UserService, public sqlite: SqliteService, public popoverCtrl: PopoverController) {
    
  }

  ngOnInit() {
    this.initializePopover();
  }

  async initializePopover(){

    this.user = this.userService.User;

    if(this.flag == 'CF'){

      var head_of_family: boolean = !!parseInt(this.user.head_of_family)
      this.can_manage_family = !!parseInt(this.user.can_manage_family) || head_of_family;
      this.can_send_passes = this.userService.canSendPasses(this.user);

      var admin = this.pid;
      this.lock_head_of_family = !!parseInt(admin.head_of_family)

    }

    if(this.flag == 'C' || this.flag == 'F'){

      var c = this.user.community;
      var h = this.user.house;
      var _c = this.pid.its_user.community
      var _h = this.pid.its_user.house
      console.log(this.pid);
      console.log(this.user);
      if(c == _c && h == _h){
        this.lock_head_of_family = !!parseInt(this.pid.its_user.head_of_family)
        //this.lock_head_of_family = true;
      }

      this.can_send_passes = this.userService.canSendPasses(this.user);
      this.can_request_passes = !!parseInt(this.pid.its_user.can_send_passes) || !!parseInt(this.pid.its_user.head_of_family);
      console.log(this.can_request_passes);

    }

    if(this.flag == 'SW'){

      this.sqlite.getAllRecords().then( data => {
        this.sw_users = data;
      })

    }
    
  }

  close(param) {
    this.popoverCtrl.dismiss({pid: this.pid, param: param });
  }


}
