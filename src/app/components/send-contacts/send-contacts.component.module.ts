import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { SendContactsComponent } from './send-contacts.component';
import { ContactitemComponentsModule } from '../contact-item/contactitem-component.module';
import { ContactselectionComponentsModule } from '../contact-selection/contactselection-component.module';
import { ContactSelectionComponent } from '../contact-selection/contact-selection.component';

@NgModule({
	entryComponents: [
		ContactSelectionComponent
	],
	declarations: [
		SendContactsComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		ContactitemComponentsModule,
		ContactselectionComponentsModule
	],
	exports: [
		SendContactsComponent
	]
})
export class SendContactComponentsModule {}
