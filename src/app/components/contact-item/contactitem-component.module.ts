import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { ContactItemComponent } from './contact-item.component';
@NgModule({
	declarations: [
		ContactItemComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		RouterModule,
		
	],
	exports: [
		ContactItemComponent
	]
})
export class ContactitemComponentsModule {}
