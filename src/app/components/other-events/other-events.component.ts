import { Component, OnInit } from '@angular/core';
import { PassesService } from 'src/app/services/passes.service';
import { ModalController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-other-events',
  templateUrl: './other-events.component.html',
  styleUrls: ['./other-events.component.scss'],
})
export class OtherEventsComponent  implements OnInit {

  userEvents: any[];
  _userEvents: any[];

  constructor(private alertCtrl: AlertController, private passService: PassesService, private modalCtrl: ModalController) { }

  ngOnInit() {
    this.getEvents();
  }

  async getEvents() {
     const evnts = await this.passService.getUserEvents() as [];
     this.userEvents = evnts;
     this._userEvents = evnts;
  }

  onSelectEvent(item){
    this.modalCtrl.dismiss(item);
  }

  async addEventToList($event, item = null){

    if (item) { $event.stopPropagation(); }
    const prompt = await this.alertCtrl.create({
      header: !item ? 'Add' : 'Edit' + ' Event',
      message: 'Enter name for this event',
      inputs: [
        {
          name: 'event_name',
          placeholder: 'Event Name',
          value: item ? item.event_name : ''
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked', data);
          }
        },
        {
          text: 'Save',
          handler: data => {

            if(item){
              item.event_name = data.event_name;
              this.passService.editUserEvent(item).then( () => {
                this.getEvents();
              });
            }else{
              item = data;
              this.passService.addUserEvent(item).then( () => {
                this.getEvents();
              });
            }

          }
        }
      ]
    });
    await prompt.present();

  }

  async showConfirm($event, item) {

    if (item) { $event.stopPropagation(); }

    const confirm = await this.alertCtrl.create({
      header: 'Are You Sure?',
      message: 'This will hide this Event from your list but passes will not be effected',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Agree',
          handler: () => {
            console.log(item);
            this.passService.deleteUserEvent(item).then( v => {
              if(v){
                this.getEvents();
              }
            })
            

          }
        }
      ]
    });
    confirm.present();
  }

  setFilteredItems($event){

    var self = this;
    const text = $event.target.value;

    this.userEvents = this._userEvents.filter((item) => {
        return (item.event_name && item.event_name.toLowerCase().indexOf(text.toLowerCase()) > -1)
    });

  }

}
