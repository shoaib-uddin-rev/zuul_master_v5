import { Component, OnInit, Input, Injector } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { BasePage } from 'src/app/pages/base-page';

@Component({
  selector: 'app-pass-detail',
  templateUrl: './pass-detail.component.html',
  styleUrls: ['./pass-detail.component.scss'],
})
export class PassDetailComponent extends BasePage implements OnInit {

  @Input() id;
  item;
  contact_added = false;

  constructor(injector: Injector, public modalCtrl: ModalController) {
    super(injector);
  }

  ngOnInit() {
    this.getDetail();
  }

  async getDetail(){
    this.item = await this.passService.getPassDetail(this.id);
    this.contact_added = (this.item.contact_id != 0);
  }

  async addToContact(item){

    let flag = await this.utility.promptAlert('Add to Contacts?', 'I Would  Like to Add <strong>' + item.sender + '</strong> to ZUUL Contacts', 'Agree', 'Disagree' );
    if(flag){
      this.contact_added = await this.contacts.addContactWithOnlyId(item.contact_id) as boolean;
    }

  }

  callMe(num){
    this.utility.dialMyPhone(num);
  }

  getDirection(address){
    console.log();

    var addr = address.trim();
    this.utility.getCoordsForGeoAddress(addr).then( coords => {
      console.log(coords);
      this.utility.getCurrentLocation(coords['lat'],coords['lng'])
    }, err => {
      this.utility.presentFailureToast("Error: Location not found");
    })

  }

  addareminder(item){

    const detailObj = {
      event_name: item.event_name,
      description: item.description,
      notes: item.phone ? "Call at: " + item.phone : "",
      startDate: Date.parse(item.to_date + ' ' + item.to_date_time),
      endDate: Date.parse(item.for_date + ' ' + item.for_date_time)
    }
    this.utility.addaremondertodate(detailObj);
  }

  close(){
    this.modalCtrl.dismiss();
  }

}
