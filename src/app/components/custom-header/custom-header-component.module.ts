import { CustomHeaderComponent } from './custom-header.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
@NgModule({
	declarations: [
		CustomHeaderComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		RouterModule,
		
	],
	exports: [
		CustomHeaderComponent
	]
})
export class CustomHeaderComponentsModule {}
