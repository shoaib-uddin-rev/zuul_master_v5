import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { GoogleMapComponent } from './google-map.component';
import { CustomHeaderComponentsModule } from '../custom-header/custom-header-component.module';

@NgModule({
	declarations: [
		GoogleMapComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		RouterModule,
		CustomHeaderComponentsModule
		
	],
	exports: [
		GoogleMapComponent
	]
})
export class GoogleMapComponentsModule {}
