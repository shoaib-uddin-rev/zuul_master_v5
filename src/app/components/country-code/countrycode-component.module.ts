import { CountryCodeComponent } from './country-code.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { ContactitemComponentsModule } from '../contact-item/contactitem-component.module';

@NgModule({
	declarations: [
		CountryCodeComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		RouterModule,
		ContactitemComponentsModule
		
	],
	exports: [
		CountryCodeComponent
	]
})
export class CountryCodeComponentsModule {}
