import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PassesPage } from './passes.page';

const routes: Routes = [
  {
    path: '',
    component: PassesPage,
    children: [
      {
        path: 'received',
        children: [
          {
            path: '',
            loadChildren: '../passes/received-passes/received-passes.module#ReceivedPassesPageModule'
          }
        ],
      },
      {
        path: 'archieve',
        children: [
          {
            path: '',
            loadChildren: '../passes/archived-passes/archived-passes.module#ArchivedPassesPageModule'
          }
        ]
        
      },
      {
        path: 'sent',
        children: [
          {
            path: '',
            loadChildren: '../passes/sent-passes/sent-passes.module#SentPassesPageModule'
          }
        ]
      },

      {
        path: '',
        redirectTo: 'received',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'received',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class PassesPageRoutingModule {}
