import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from '../base-page';

@Component({
  selector: 'app-passes',
  templateUrl: './passes.page.html',
  styleUrls: ['./passes.page.scss'],
})
export class PassesPage extends BasePage implements OnInit {
  MyHeaderComponentModule
  pass_type = 'received';
  archived_pass_type = 'archivedPasses';
  activePasses : any[] = [];
  archivePasses : any[] = [];
  constructor(injector: Injector) {
    super(injector);
    
   }

  ngOnInit() {
    // this.initActivePasses();
    // this.initArchivePasses();
  }

//  async initActivePasses() {
//   this.activePasses = await this.passService.getActivePasses(this.userService.User.id) as [];
//  console.log(this.activePasses);
//  }

//  async initArchivePasses() {
//   this.archivePasses = await this.passService.getUserArchivePasses(this.userService.User.id) as [];
//   console.log(this.archivePasses);
//  }


}
