import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page';

@Component({
  selector: 'app-sent',
  templateUrl: './sent.page.html',
  styleUrls: ['./sent.page.scss'],
})
export class SentPage extends BasePage implements OnInit {

  archiveSentPasses : any[] = [];
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.initArchiveSentPasses();
  }

  async initArchiveSentPasses() {
    this.archiveSentPasses = await this.passService.getArchiveSentPasses(this.userService.User.id) as [];
  }

}
