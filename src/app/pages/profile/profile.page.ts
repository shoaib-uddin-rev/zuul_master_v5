import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from '../base-page';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CountryCodeComponent } from 'src/app/components/country-code/country-code.component';
import { ModalController } from '@ionic/angular';
import { CropperComponent } from 'src/app/components/cropper/cropper.component';
import { CountryflagService } from 'src/app/services/countryflag.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage extends BasePage implements OnInit {

  new: boolean = false;
  aForm: FormGroup;

  dial_code = { name: 'United States', dial_code: '+1', code: 'US', image: 'assets/imgs/flags/us.png' };

  constructor(injector: Injector, public formBuilder: FormBuilder, private flags: CountryflagService, public modalCtrl: ModalController) {
    super(injector);
    this.setupForm();
  }

  ngOnInit() {
    this.fetchProfileValues();
  }

  setupForm() {
    var re = /\S+@\S+\.\S+/;

    this.aForm = this.formBuilder.group({
      full_name: [
        '',
        Validators.compose([
          Validators.pattern('[a-zA-Z ]*'),
          Validators.required
        ])
      ],
      name: [
        '',
        Validators.compose([
          Validators.pattern('[a-zA-Z ]*'),
          Validators.required
        ])
      ],
      middle_name: ['', Validators.compose([Validators.pattern('[a-zA-Z ]*')])],
      last_name: [
        '',
        Validators.compose([
          Validators.pattern('[a-zA-Z ]*'),
          Validators.required
        ])
      ],
      date_of_birth: ['', Validators.compose([Validators.required])],
      phone_number: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(10),
          Validators.pattern('[0-9]*')
        ])
      ],
      email: [
        '',
        Validators.compose([
          Validators.pattern(re),
          Validators.required
        ]) /*, VemailValidator.checkEmail */
      ],

      apartment: [''],
      street_address: [''],
      city: ['', Validators.compose([Validators.required])],
      state: ['', Validators.compose([Validators.required])],
      zip_code: ['', Validators.compose([Validators.required, Validators.pattern('[0-9]*')])],
      dial_code: ['+1'],
      licence_format: ['si', Validators.compose([Validators.required])],
      licence_image: ['assets/imgs/pass.png', Validators.compose([Validators.required])],
      profile_image: [this.userService.avatar]
    });
  }

  async fetchProfileValues() {

    // get user data
    const user = await this.userService.User;

    if (user) {
      // redirect to welcomeid
      const profile = user.profile;
      console.log(profile);

      this.aForm.controls.name.setValue(user.name);
      this.aForm.controls.middle_name.setValue(profile.middle_name);
      this.aForm.controls.last_name.setValue(profile.last_name);
      const fullname = this.utility.combineFullName(user.name, profile.middle_name, profile.last_name);
      this.aForm.controls.full_name.setValue(fullname);
      this.aForm.controls.date_of_birth.setValue(profile.date_of_birth);
      const _tel = this.utility.onkeyupFormatPhoneNumberRuntime(user.phone_number);
      this.aForm.controls.phone_number.setValue(_tel);

      if(!user.dial_code) { user.dial_code = '+1'; }
      this.aForm.controls.dial_code.setValue(user.dial_code);

      this.aForm.controls.email.setValue(user.email);
      this.aForm.controls.apartment.setValue(profile.apartment);
      this.aForm.controls.street_address.setValue(profile.street_address);
      this.aForm.controls.city.setValue(profile.city);
      this.aForm.controls.state.setValue(profile.state);
      this.aForm.controls.zip_code.setValue(profile.zip_code);
      this.aForm.controls.licence_format.setValue(profile.licence_format);
      this.aForm.controls.licence_image.setValue(profile.licenceImageUrl);
      this.aForm.controls.profile_image.setValue(user.profileImageUrl);

      //
      // do code search
      this.setDialCode(user);
    }
      
  }

  async setDialCode(user) {
    await this.flags.setFlagsArray();
    this.dial_code = this.flags.countries.find(x => x.dial_code == user.dial_code);
  }

  onTelephoneChange(ev) {
    if (ev.inputType != 'deleteContentBackward') {
      const formattedString = this.utility.onkeyupFormatPhoneNumberRuntime(ev.target.value);
      this.aForm.controls['phone_number'].setValue(formattedString);
    }
  }

  openStateSelection() {
    this.openCountryModal('states');
  }

  getCountryCode() {
    this.openCountryModal('flags');
  }

  async openCountryModal(type) {
    const modal = await this.modalCtrl.create({
      component: CountryCodeComponent,
      componentProps: {
        type
      }

    });
    modal.onDidDismiss().then(data => {
      console.log(data);
      if (type == 'flags') {
        this.dial_code = data['data'] as any;
        this.aForm.controls['dial_code'].setValue(this.dial_code['dial_code']);
      }

      if (type == 'states') {
        const obj = data['data'] as any;
        this.aForm.controls['state'].setValue(obj['name']);
      }

    })
    return await modal.present();
  }

  onZipCodeKeyUp(event) {
    console.log(event.target.value);
    var e = event.target.value;
    e = e.replace(/\D/g, '');
    console.log(e);
    event.target.value = e;
    this.aForm.controls['zip_code'].setValue(e);
  }

  getprofileimage() {
    this.utility.snapImage('profile').then(imageData => {
      if (imageData) {
        this.cropWithController('data:image/jpeg;base64,' + imageData, 'profile')
          .then(image => {
            if (image) {
              this.aForm.controls['profile_image'].setValue(image);
            }
          });
      }

    });
  }

  getlicanceimage() {
    this.utility.snapImage('licence').then(imageData => {
      if (imageData) {
        this.cropWithController('data:image/jpeg;base64,' + imageData, 'licence')
          .then(image => {
            if (image) {
              this.aForm.controls['licence_image'].setValue(image);
            }
          });
      }

    });
  }

  cropWithController(imageData, type) {

    return new Promise(async resolve => {
      const modal = await this.modalCtrl.create({
        component: CropperComponent,
        componentProps: {
          image: imageData,
          aspectRatio: (type == 'licence') ? 16 / 9 : 1
        }
      });

      modal.onDidDismiss().then(_data => {
        const data = _data['data'];
        resolve(data);
      });

      modal.present();
    })


  }

  async validate_form(){

    const eror = 'Please Enter Valid';
    const fullname = this.utility.seperateFullName(this.aForm.controls.full_name.value);
    this.aForm.controls.name.setValue(fullname.first_name);
    this.aForm.controls.middle_name.setValue(fullname.middle_name);
    this.aForm.controls.last_name.setValue(fullname.last_name);

    const in_name = !this.aForm.controls.name.valid || !this.utility.isLastNameExist(this.aForm.controls.name.value);
    if (in_name) {
      this.utility.presentFailureToast(eror + ' Name, can not be null, only alphabets allowed')
      return false;
    }

    const in_lname = !this.aForm.controls.last_name.valid;
    if (in_lname) {
      this.utility.presentFailureToast(eror + ' Last Name, can not be null, only alphabets allowed');
      return false;
    }

    const in_email = !this.aForm.controls.email.valid;
    if (in_email) {
      this.utility.presentFailureToast(eror + ' Email');
      return false;
    }

    await this.userService.isEmailExistInSystem(this.aForm.controls.email.value);

    const in_birth = !this.aForm.controls.date_of_birth.valid;
    if (in_birth) {
      this.utility.presentFailureToast(eror + ' Date Of Birth');
      return false;
    }

    var in_dialcode = !this.aForm.controls.dial_code.valid;
    if (in_dialcode) {
      this.utility.presentFailureToast(eror + ' Dial Code')
      return false;
    }

    var in_phone = !this.aForm.controls.phone_number.valid;
    in_phone = !this.utility.isPhoneNumberValid(this.aForm.controls.phone_number.value);
    if (in_phone) {
      this.utility.presentFailureToast(eror + ' Phone Number (Minimum 10 Digits)');
      return false;
    }

    var in_address = !this.aForm.controls.street_address.valid;
    if (in_address) {
      this.utility.presentFailureToast(eror + ' Street Address');
      return false;
    }
    
    var in_city = !this.aForm.controls.city.valid;
    if (in_city) {
      this.utility.presentFailureToast(eror + ' city');
      return false;
    }
    
    var in_state = !this.aForm.controls.state.valid;
    if (in_state) {
      this.utility.presentFailureToast(eror + ' state');
      return false;
    }

    var in_zip_code = !this.aForm.controls.zip_code.valid;
    if (in_zip_code) {
      this.utility.presentFailureToast(eror + ' zip code');
      return false;
    }

    console.log(this.aForm.controls.licence_image.value);
    var in_licence = !this.aForm.controls.licence_image.valid || this.aForm.controls.licence_image.value.indexOf('pass.png') > -1;
    if (in_licence) {
      this.utility.presentFailureToast(eror + ' licence image');
      return false;
    }

    return true;

  }

  async update(){

    const flag = await this.validate_form() as boolean;
    if (flag) {
      const formdata = this.aForm.value;
      this.network.updateProfile(formdata).subscribe( async res => {
        this.utility.presentSuccessToast(res['message']);
        this.userService.getUser();
        this.nav.pop();
        

      }, err => {});
    }


  }

}
