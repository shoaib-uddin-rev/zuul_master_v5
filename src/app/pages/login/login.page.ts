import { Component, OnInit, Injector } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BasePage } from '../base-page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage extends BasePage {

  aForm: FormGroup;
  bForm: FormGroup;
  cForm: FormGroup;
  submitAttempt = false;
  formtype = 'login';
  
  constructor(injector: Injector,
    public formBuilder: FormBuilder,
    
    ) {
    super(injector);
    this.setupForm();
    this.formtype = this.getQueryParams().formtype ? this.getQueryParams().formtype : 'login' ;
  }

  setupForm(){

    this.aForm = this.formBuilder.group({
      phone_number: ['', Validators.compose([Validators.required]) /*, VemailValidator.checkEmail */ ],
      password: ['', Validators.compose([Validators.minLength(6),Validators.maxLength(30), Validators.required])]
    });

    this.bForm = this.formBuilder.group({
      email: [''],
      name: ['', Validators.compose([Validators.minLength(3), Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      phone_number: ['', Validators.compose([ Validators.required]) ],
      password: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(30), Validators.required])],
      password_confirm: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(30), Validators.required])],
      profile_image: ['']
    }, { validator: this.utility.checkIfMatchingPasswords('password', 'password_confirm') });

    this.cForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.email, Validators.required])]
    });

  }

  onLogin(){
    this.submitAttempt = true;
    let in_phone = !this.aForm.controls.phone_number.valid;
    in_phone = !this.utility.isPhoneNumberValid(this.aForm.controls.phone_number.value);
    const in_password = !this.aForm.controls.password.valid;

    if(in_phone){
      this.utility.presentFailureToast('Valid Phone Number required');
      return;
    }

    if(in_password){
      this.utility.presentFailureToast('Valid password (minimum 6 characters) required');
      return;
    }

    var formdata = this.aForm.value;

    formdata['register_with_phonenumber'] = true;
    this.events.publish('user:login', formdata);
    


    
  }

  onSignup(){

    this.submitAttempt = true;

    const in_name = !this.bForm.controls.name.valid || !this.utility.isLastNameExist(this.bForm.controls.name.value);
    let in_phone = !this.bForm.controls.phone_number.valid;
    in_phone = !this.utility.isPhoneNumberValid(this.bForm.controls.phone_number.value);
    const in_password = !this.bForm.controls.password.valid;
    const in_cpassword = !this.bForm.controls.password_confirm.valid;

    if(in_name){
      this.utility.presentFailureToast('Name/Full Name is required');
      return;
    }

    if(in_phone){
      this.utility.presentFailureToast('Phone Number required');
      return;
    }

    if(in_password){
      this.utility.presentFailureToast('Valid password (minimum 6 characters) required');
      return;
    }

    var _p = this.bForm.controls.password.value;
    var _cp = this.bForm.controls.password_confirm.value;


    if(_p != _cp){
      this.utility.presentFailureToast('confirm password must match password field');
      return;
    }

    var formdata = this.bForm.value;
    formdata['profile_image'] = null;

    formdata['register_with_phonenumber'] = true;
    this.network.register(formdata).subscribe(() => {
      formdata['showelcome'] = true;

      this.events.publish('user:login', formdata);
    }, err => { })

  }

  onForgetPassword(){

    var in_email = !this.cForm.controls.email.valid
    if(in_email){
      this.utility.presentFailureToast("Email address is invalid, please enter a valid email address");
      return
    }
    this.submitAttempt = true;
    var formdata = this.cForm.value;

    this.network.forgetPassword(formdata).subscribe( res => {

      if(res["bool"] == true){
        this.utility.presentSuccessToast(res['message']);
      }else{
        this.utility.presentFailureToast(res['message']);
      }

    }, err => { })

  }

  onTelephoneChange(ev, type) {
    if (ev.inputType != 'deleteContentBackward') {
      
      const formattedString = this.utility.onkeyupFormatPhoneNumberRuntime(ev.target.value);
      (type == 'A') ? this.aForm.controls['phone_number'].setValue(formattedString) : this.bForm.controls['phone_number'].setValue(formattedString)
    }
  }

  redirectToContactUsFOrm(){
    this.utility.openContactFormUrl();
  }

}
