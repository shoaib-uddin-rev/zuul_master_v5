import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from '../base-page';
import { ModalController } from '@ionic/angular';
import { PassDetailComponent } from 'src/app/components/pass-detail/pass-detail.component';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage extends BasePage implements OnInit {

  plist: any[] = [];

  constructor(injector: Injector, public modalCtrl: ModalController) {
    super(injector);
  }

  ngOnInit() {
    this.initializeDashboard();
  }

  async initializeDashboard(){
    console.log(this.userService.User);
    this.userService.setTokenToServer();
    this.plist = await this.passService.getActivePasses(this.userService.User.id) as [];
    await this.contacts.getContactsData();
    
  }

  async passDetails($event, item){
    console.log($event, item);
    const modal = await this.modalCtrl.create({
      component: PassDetailComponent,
      componentProps: {
        id : item.its_active_pass.id
      }
    });
    return await modal.present();

  }

  // async loadData(event: any = {}) {
  //   this.refresher = event.target;
  //   this.isEmptyViewVisible = false;
  //   await this.getActivePasses()
  //   if (this.refresher) {
  //     this.refresher.complete();
  //   }
  // }

  // getActivePasses() {
  //   return new Promise((resolve) => {
  //     this.network.getUserReceivedPasses(this.userService.User.id).subscribe((data: any) => {
  //       console.log(data);
  //       this.plist = data['passes'];
  //       if(this.plist.length == 0){
  //         this.isEmptyViewVisible = true;
  //       }else{
  //         this.isEmptyViewVisible = false;
  //       }
  //       resolve();
  //     }, err => { })
  //   })
  // }

}
