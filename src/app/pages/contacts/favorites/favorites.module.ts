import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FavoritesPage } from './favorites.page';
import { ContactitemComponentsModule } from 'src/app/components/contact-item/contactitem-component.module';
import { ScrollAlphaComponent } from 'src/app/components/scroll-alpha/scroll-alpha.component';
import { ScrollalphaComponentsModule } from 'src/app/components/scroll-alpha/scrollalpha-component.module';

const routes: Routes = [
  {
    path: '',
    component: FavoritesPage
  }
];

@NgModule({
  entryComponents: [
   ScrollAlphaComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ContactitemComponentsModule,
    ScrollalphaComponentsModule
  ],
  declarations: [FavoritesPage]
})
export class FavoritesPageModule {}
