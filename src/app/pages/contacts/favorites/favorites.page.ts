import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from '../../base-page';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.page.html',
  styleUrls: ['./favorites.page.scss'],
})
export class FavoritesPage extends BasePage implements OnInit {
  fav_contacts: any[] = [];
  _fav_contacts: any[] = [];
  constructor(injector: Injector) { 
    super(injector);
   }

  ngOnInit() {
    this.initializeContacts();
  }

  async initializeContacts(){
    const data = await this.contacts.getFavoritesData() as [];
    console.log(data);
    this.fav_contacts = data;
    this._fav_contacts = data;
  }



  
  async removeFromFavorites(item){
    await this.contacts.removeFromFavorites(item);
    const index = this.fav_contacts.findIndex(x => x.id == item.id);
    this.fav_contacts.splice(index, 1);
    this.fav_contacts = [...this.fav_contacts];
  }
  
  favoritesSwitch(data){
    console.log(data, data["param"])
    var item = data["pid"];
    switch (data["param"]) {
      case "R":
        this.removeFromFavorites(item)
        break;
      case "G":
        //this.createSelectedInContactGroupByItem([item]);
        break;
      case "E":
        //this.editFamily(item.user_id);
        break;
      case "C":
        //this.callMe(item.phone_number);
        break;
      case "D":
        // item.selected = true;
        // this.removeFromFavorites(item);
        break;
      case "SP":
        //this.fetchEventsToSendPassTo(item);
        break;
      case "RP":
        //this.sendPassRequestToContact(item);
        break;
    }
  }
  filterByAlpha($event){
    const alpha = $event.alpha;
    this.fav_contacts = this._fav_contacts.filter((item) => {
      return (!alpha) ? true : item.display_name.toLowerCase().charAt(0) == alpha.toLowerCase();
      
    });
    console.log(this.fav_contacts);
  }
}
