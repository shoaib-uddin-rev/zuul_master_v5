import { EmptyviewComponentsModule } from './../../components/empty-view/emptyview-component.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ContactsPage } from './contacts.page';
import { CustomHeaderComponentsModule } from 'src/app/components/custom-header/custom-header-component.module';
import { ScrollalphaComponentsModule } from 'src/app/components/scroll-alpha/scrollalpha-component.module';
import { ContactitemComponentsModule } from 'src/app/components/contact-item/contactitem-component.module';
import { PopoverComponentsModule } from 'src/app/components/popover/emptyview-component.module';
import { ContactsPageRoutingModule } from './contacts.router.module';

const routes: Routes = [
  {
    path: '',
    component: ContactsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    // RouterModule.forChild(routes),
    ContactsPageRoutingModule,
    CustomHeaderComponentsModule,
    ScrollalphaComponentsModule,
    EmptyviewComponentsModule,
    ContactitemComponentsModule,
    PopoverComponentsModule
  ],
  declarations: [ContactsPage]
})
export class ContactsPageModule {}
