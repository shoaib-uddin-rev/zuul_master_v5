import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from '../../base-page';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.page.html',
  styleUrls: ['./groups.page.scss'],
})
export class GroupsPage extends BasePage implements OnInit {

  group_contacts: any[] = [];
  _group_contacts: any[] = [];

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.initializeGroups();
  }

  async initializeGroups(){
    const data = await this.contacts.getGroups() as [];
    this.group_contacts = data;
    this._group_contacts = data;
  }

}
