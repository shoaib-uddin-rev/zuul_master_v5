import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ContactsPage } from './contacts.page';
import { ContactitemComponentsModule } from 'src/app/components/contact-item/contactitem-component.module';
import { AddContactComponent } from 'src/app/components/add-contact/add-contact.component';
import { AddContactComponentsModule } from 'src/app/components/add-contact/addcontact-component.module';
import { ContactSelectionComponent } from 'src/app/components/contact-selection/contact-selection.component';
import { ContactselectionComponentsModule } from 'src/app/components/contact-selection/contactselection-component.module';
import { ScrollalphaComponentsModule } from 'src/app/components/scroll-alpha/scrollalpha-component.module';
import { ScrollAlphaComponent } from 'src/app/components/scroll-alpha/scroll-alpha.component';

const routes: Routes = [
  {
    path: '',
    component: ContactsPage
  }
];

@NgModule({
  entryComponents: [
    AddContactComponent, 
    ContactSelectionComponent,
    ScrollAlphaComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ContactitemComponentsModule,
    AddContactComponentsModule,
    ContactselectionComponentsModule,
    ScrollalphaComponentsModule
  ],
  declarations: [ContactsPage]
})
export class ContactsPageModule {}
