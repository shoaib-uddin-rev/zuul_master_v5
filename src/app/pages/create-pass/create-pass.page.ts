import { Component, OnInit, Injector, Input } from '@angular/core';
import { BasePage } from '../base-page';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { OtherEventsComponent } from 'src/app/components/other-events/other-events.component';
import { ContactSelectionComponent } from 'src/app/components/contact-selection/contact-selection.component';
import { GoogleMapComponent } from 'src/app/components/google-map/google-map.component';

@Component({
  selector: 'app-create-pass',
  templateUrl: './create-pass.page.html',
  styleUrls: ['./create-pass.page.scss'],
})
export class CreatePassPage extends BasePage implements OnInit {
  // @Input() id; 
  aForm: FormGroup;
  dateOfPassMdy;
  endDateOfPassMdy;
  phone_contacts: any[] = [];
  userEvents: any[] = [];
  contactId: any[] = [];
  _contactId: any[] = [];

  constructor(injector: Injector, public formBuilder: FormBuilder, private modalCtrl: ModalController) {
    super(injector);
    this.setupForm();
  }

  ngOnInit() {
    // this.addContactsToArray(this.getQueryParams());
  }
  ionViewWillEnter(){
    this.initContactService();
  }
  async initContactService(){
    console.log("Call the service");
    var sendPassTo = this.getQueryParams().send_pass_to;
    if(sendPassTo){
      sendPassTo = sendPassTo.split(',');
      if(sendPassTo.length > 0){
        this.phone_contacts = [...this.contacts.getContactsForIds(sendPassTo)];
      }
    }
  }

  setupForm() {

    this.aForm = this.formBuilder.group({
      pass_date: ['', Validators.compose([Validators.required])],
      pass_end_date: [{value: '', disabled: true}],
      pass_validity: ['24', Validators.compose([Validators.required])],
      pass_type: ['1', Validators.compose([Validators.required])],
      visitor_type: ['1', Validators.compose([Validators.required])],
      description: ['Default Location not found', Validators.compose([Validators.required])],
      event_id: ['', Validators.compose([Validators.required])],
      event_name: [''],
      pass_image: ['', Validators.compose([Validators.required])],
      
      lat: [''],
      lng: [''],
    });

    const address = this.utility.parseAddressFromProfile(this.userService.User.profile);
    this.aForm.controls.description.setValue(address);
    
    this.initSelectedEvent();

  }

  onSelectChange(ev){
    const val = this.aForm.controls.pass_type.value;
    if (val == 1) {
      this.aForm.controls.pass_validity.setValue(24);
    }

    if(val == 3){
      const me = this.userService.User;
      const selfcontact = {
        id: me.id,
        user_id: me.id,
        display_name: me.fullName,
        phone_number: me.phone_number
      }
      this.phone_contacts = [selfcontact];
      

    }

  }

  setEndTimeAfterValidityChange(ev) {
    let v = this.aForm.controls.pass_date.value;
    if (v == null || v == '') { return; };
    this.setFutureDate();
  }

  setDate($event){

    //this.aForm.controls.pass_date.setValue(this.utility.formatDateTime($event.target.value));
    this.setFutureDate();

  }

  async setFutureDate() {
    const pdate = this.utility.formatDateTime(this.aForm.controls.pass_date.value);
    const dataObj = { date: pdate, validity: this.aForm.controls.pass_validity.value }
    const endDate = await this.passService.getAddedHourDate(dataObj);
    this.aForm.controls.pass_end_date.setValue(endDate);
  }

  async initSelectedEvent() {
    const ev = await this.passService.getDefaultEvent();
    this.userEvents[0] = ev;
    // console.log(ev);
    this.aForm.controls.event_id.setValue(ev.event_id);
    this.aForm.controls.event_name.setValue(ev.event_name);
  }

  async openEventSelection(){

    var self = this;
    const modal = await this.modalCtrl.create({
      component: OtherEventsComponent
    });
    modal.onDidDismiss().then( _data => {
      const data = _data['data'];
      if (data) {
        self.userEvents[0] = data;
        this.aForm.controls.event_id.setValue(data.id);
        this.aForm.controls.event_name.setValue(data.event_name);
        this.passService.setEventToStorage(data);
      }
    });
    return await modal.present();
  }

  async openContactSelection(){
    let self = this;
    const modal = await this.modalCtrl.create({
      component: ContactSelectionComponent,
      componentProps: {
        'fromContacts': true,
      }

    });
    modal.onDidDismiss().then( _data => {
      const data = _data['data'];
      console.log(data);
      if(data){
        self.addContactsToArray(data);
      }
      
    });
    return await modal.present();
  }

  async openLocation(){
    let self = this;
    const modal = await this.modalCtrl.create({
      component: GoogleMapComponent,
      componentProps: {
        'location': self.aForm.controls.description.value
      }

    });
    modal.onDidDismiss().then( _data => {
      const data = _data['data'];
      console.log(data);
      if(data){
        if(data){
          self.aForm.controls.lat.setValue(data.lat);
          self.aForm.controls.lng.setValue(data.lng);
          self.aForm.controls.description.setValue(data.address);
        }
      }
      
    });
    return await modal.present();
  }



  addContactsToArray(data){
    data.forEach(element => {
      const index = this.phone_contacts.findIndex(x => x.id == element.id);
      console.log(index);
      if(index == -1){
        console.log("here");
        this.phone_contacts.push(element);
        this.phone_contacts.sort((a, b) => (a['display_name'] > b['display_name']) ? 1 : ((b['display_name'] > a['display_name']) ? -1 : 0));
        this.phone_contacts = [...this.phone_contacts];
      }
    });
  }

  removeContact($event, item) {
    const index = this.phone_contacts.findIndex(x => x.id == item.id);
    console.log(index);
    if (index > -1) {
      this.phone_contacts.splice(index, 1);
      this.phone_contacts = [...this.phone_contacts];
    }
  }

  removeAllContactSelection(){
    this.phone_contacts = [];
  }

  async create() {

    var formdata = this.aForm.value;

    

    if(!this.aForm.controls.pass_date.valid){
      this.utility.presentFailureToast("Please Select Pass Date");
      return; 
    }

    if(this.phone_contacts.length == 0){
      this.utility.presentFailureToast("Please Add Contacts to Pass");
      return
    }

    if(this.phone_contacts.length > 1 && this.aForm.controls['pass_type'].value == 2 ){
      this.utility.presentFailureToast("only one contact can be selected for recurring");
      return;
    }


    formdata['pass_date'] = this.utility.formatDateTime(formdata['pass_date']);
    formdata['selected_contacts'] = this.phone_contacts.map(x => x.user_id);
    console.log(formdata['selected_contacts']);

    const flag = await this.passService.createPass(formdata);
    this.aForm.reset();

    if(flag){
      this.showSuccessAlert(formdata);
      this.phone_contacts = [];
    }
    
    


  }

  async showSuccessAlert(formdata){
    
    let heading = 'Pass Has Been Sent';
    let message = 'A pass invite along with a unique invitation code has been sent to all your contacts.'
    
    if(parseInt(formdata['pass_type']) == 3){

      heading = 'Pass Created';
      message = 'Pass has been sent to self.';
    }

    message = message + ' Would you like to Add to calender as well?'
    
    let flag = await this.utility.promptAlert(heading, message, 'Sure', 'Not Now')
    if(flag){
      // add calender function
      this.addareminder(formdata)

    }
    

  }

  addareminder(reminderObject){
    const detailObj = {
      event_name: reminderObject.event_name,
      description: reminderObject.description,
      notes: "",
      startDate: Date.parse(reminderObject.pass_date),
      endDate: Date.parse(reminderObject.pass_end_date)
    }
    this.utility.addaremondertodate(detailObj);
  }

  getDirection(address){

    var addr = address.trim();
    this.utility.getCoordsForGeoAddress(addr).then( coords => {
      console.log(coords);
      this.utility.getCurrentLocation(coords['lat'],coords['lng'])
    }, err => {
      this.utility.presentFailureToast("Error: Location not found");
    })

  }

}
