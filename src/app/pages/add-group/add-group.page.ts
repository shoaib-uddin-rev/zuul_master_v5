import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from '../base-page';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-group',
  templateUrl: './add-group.page.html',
  styleUrls: ['./add-group.page.scss'],
})
export class AddGroupPage extends BasePage implements OnInit {

  aForm: FormGroup;
  id;

  constructor(injector: Injector, public formBuilder: FormBuilder) {
    super(injector);
    this.setupForm()
  }

  ngOnInit() {
    this.id = this.getQueryParams().id;
    if(this.id){
      this.setFormValues()
    }
  }

  async setFormValues(){
    let group = await this.contacts.getGroupById(this.id)
    this.aForm.controls['group_name'].setValue(group['group_name']);
    this.aForm.controls['group_description'].setValue(group['group_description']);
  }

  setupForm(){

    this.aForm = this.formBuilder.group({
      group_name: ['', Validators.compose([Validators.required]) ],
      group_description: ['']
    })

  }

  async create(){

    var in_name = !this.aForm.controls.group_name.valid;

    if(in_name){
      this.utility.presentFailureToast("group name is required")
      return;
    }

    var formdata = this.aForm.value;
    if(this.id){
      await this.contacts.updateOldGroup(this.id, formdata);
    }else{
      await this.contacts.createNewGroup(formdata);
    }

    this.events.publish('contacts:reloadgroups')
    this.nav.pop();

    
    

  }


}
