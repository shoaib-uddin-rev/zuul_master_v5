import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RequestPassPage } from './request-pass.page';
import { CustomHeaderComponentsModule } from 'src/app/components/custom-header/custom-header-component.module';

const routes: Routes = [
  {
    path: '',
    component: RequestPassPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    CustomHeaderComponentsModule
  ],
  declarations: [RequestPassPage]
})
export class RequestPassPageModule {}
