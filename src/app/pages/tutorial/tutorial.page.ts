import { Router } from '@angular/router';
import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { BasePage } from '../base-page';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.page.html',
  styleUrls: ['./tutorial.page.scss'],
})
export class TutorialPage extends BasePage {

  @ViewChild(IonSlides) slides: IonSlides;
  slideOpts = [];
  showSkip = true;
  assetlink = 'assets/imgs/';

  slideData: any = [
    {
      heading: 'Welcome to',
      headingbold: 'ZUUL',
      image: 'tutorial.png',
      content: 'Zuul’s state of the art guest management system is designed specifically for your safety and convenience.  Whether a resident or a guest, Zuul provides you with all the necessary tools for hassle-free guest registry and entry.'
    },
    {
      heading: 'Contacts, Passes,',
      headingbold: 'and Monitoring',
      image: 'tutorial-2.png',
      content: 'Easily import your contacts from your mobile device. Send passes to individuals or groups through a secure channel. Edit or cancel a pass with ease. Zuul’s mobile app gives you the ability to manage your passes anytime anywhere.'
    },
    {
      heading: 'Highly Secure and',
      headingbold: 'Convenient Guest Verification',
      image: 'tutorial-3.png',
      content: 'Having to dig around for your driver’s license at the gate is now a thing of the past. All passes display the guest’s ID, the sender’s information, and the QR code right on their mobile device.'
    },
    {
      heading: 'Real-time Guest',
      headingbold: 'Scanning and Notification',
      image: 'tutorial-4.png',
      content: 'Zuul is designed for everyone. Guards no longer need to waste time going back and forth into the guard house, long wait times at the gate are significantly reduced for guests, and residents can enjoy a convenient, easy to use guest management system.'
    }
  ]

  constructor(injector: Injector) {
    super(injector);
  }

  async goToNextSlide(){
    const isEnd = await this.slides.isEnd();
    if(isEnd){
        this.signup()
    }else{
      this.slides.slideNext();
    }
  }

  onSlideChangeStart(slider) {
      this.showSkip = !slider.isEnd();
  }

  openLoginPage(){
    this.navigateTo('login');
  }

  signup(){
    this.navigateTo('login', {formtype: 'signup'});
  }

}
