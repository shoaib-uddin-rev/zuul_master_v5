import { Component, OnInit } from '@angular/core';
import { UtilityService } from 'src/app/services/utility.service';
import { Platform, Events } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.page.html',
  styleUrls: ['./splash.page.scss'],
})
export class SplashPage implements OnInit {

  tapCount = 0;
  skipintro = false;
  stimeout;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private utility: UtilityService,
    private router: Router,

  ) { }

  ngOnInit() {
  }

  tap() {
    this.tapCount = this.tapCount + 1;
    setTimeout(function() {
        this.tapCount = 0;
      }, 500);
    if (this.tapCount == 2) {

      if (!this.skipintro) {
        this.skipintro = true;
        this.tapCount = 0;
        this.utility.presentToast('Skipping Intro ...');
        clearTimeout(this.stimeout);
        this.callRedirect();
      }

    }
  }

  ionViewDidEnter() {
    this.platform.ready().then(() => {
      this.splashScreen.hide();
    });
    // this.menu.swipeEnable(false);
    this.init();
  }

  private async init() {

    // get all data from local storage
    const self = this;
    this.stimeout =  setTimeout(() => {self.callRedirect(); }, 6000);

  }

  async callRedirect() {
    this.router.navigate(['tabs']);
  }

}
