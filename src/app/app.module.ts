import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy, IonRefresher } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SidemenuComponent } from './components/sidemenu/sidemenu.component';
import { IonicStorageModule } from '@ionic/storage';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { FCM } from '@ionic-native/fcm/ngx';
import { InterceptorService } from './services/interceptor.service';
import { EmptyviewComponentsModule } from './components/empty-view/emptyview-component.module';
import { SidemenuComponentsModule } from './components/sidemenu/sidemenu-component.module';
import { PopoverComponent } from './components/popover/popover.component';
import { PopoverComponentsModule } from './components/popover/emptyview-component.module';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { Calendar } from '@ionic-native/calendar/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { NgxCropperjsModule } from 'ngx-cropperjs';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [SidemenuComponent, PopoverComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    SidemenuComponentsModule,
    EmptyviewComponentsModule,
    PopoverComponentsModule,
    NgxCropperjsModule,
    NgxQRCodeModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SQLite,
    InAppBrowser,
    FCM,
    CallNumber,
    Contacts,
    Keyboard,
    Calendar,
    Diagnostic,
    OpenNativeSettings,
    Camera,
    LaunchNavigator,
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true },
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
