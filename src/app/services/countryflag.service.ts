import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CountryflagService {

  countries: any[] = [];
  states: any[] = [];
  constructor(private http: HttpClient) {}

  setFlagsArray(){
    return new Promise( resolve => {
      this.http.get('assets/countries.json').subscribe(data => {
        this.countries = data as any;
        this.countries.forEach(element => {
          const imglink = 'assets/imgs/flags/' + element.code.toLowerCase() + '.png';
          element.image = imglink;
        });
        resolve();
      });
    });
  }

  setUsStates(){
    return new Promise( resolve => {
      this.http.get('assets/us_states.json').subscribe(data => {
        this.states = data as any;
        resolve();
      });
    })
    
  }

  getFlags(){
    return this.countries;
  }

  getUsStates(){
    return this.states;
  }


}
