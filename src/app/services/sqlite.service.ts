import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject, SQLiteDatabaseConfig } from '@ionic-native/sqlite/ngx';
import { Storage } from '@ionic/storage';

interface UserModel {
  name: string,
  email: string,
  phone_number: string,
  profile_image: string,
  token: string,
  fcm_token: string,
  active: number,
  can_be_resident: number,
}

@Injectable({
  providedIn: 'root'
})
export class SqliteService {

  noSQLObj = [];
  config: SQLiteDatabaseConfig = {
    name: 'zuul.db',
    location: 'default'
  }
  database: SQLiteObject;

  constructor(private sqlite: SQLite, private storage: Storage, private platform: Platform) {
  }

  private isDevice() {
    //return this.platform.is('cordova')
    return false; 
  }

  createTable() {

    if (this.isDevice()) {
      // tslint:disable-next-line: max-line-length
      const sql = 'CREATE TABLE IF NOT EXISTS users( name TEXT, email TEXT, phone_number TEXT UNIQUE, profile_image TEXT, token TEXT, fcm_token TEXT, active INTEGER DEFAULT 0, can_be_resident INTEGER DEFAULT 0,  )';
      this.sqlite.create(this.config)
        .then(db => {
          this.database = db;
          db.executeSql(sql, [])
        })
        .catch(err => console.error(err));
    }else{
      this.storage.set('user', JSON.stringify(this.noSQLObj));
    }

  }

  getAllRecords() {
    if (this.isDevice()) {
      return new Promise(resolve => {
        this.database.executeSql("SELECT * FROM users", []).then(result => {
          resolve(this.getRows(result))
        }).catch(err => { console.error(err); resolve([]) })
      })
    } else {
      return new Promise(resolve => {
        this.storage.get('user').then( v => {
          resolve(JSON.parse(v));
        })
        
      })

    }

  }

  addRecord(name, email, phone_number, profile_image) {
    
    if (this.isDevice()) {
      return new Promise(resolve => {
        this.database.executeSql("INSERT OR REPLACE INTO users (name, email, phone_number, profile_image) VALUES (?,?,?,?) ", [name, email, phone_number, profile_image]).then(() => resolve(), err => { console.error(err), resolve() })
      });
    } else {
      return new Promise(resolve => {
        this.storage.get('user').then( v => {
          
          if(!v){
            this.noSQLObj.push({ name, email, phone_number, profile_image });
          }else{
            this.noSQLObj = JSON.parse(v);
            const rindex = this.noSQLObj.findIndex(x => x.phone_number == phone_number);
            if (rindex != -1) {
              this.noSQLObj[rindex]["name"] = name;
              this.noSQLObj[rindex]["email"] = email;
              this.noSQLObj[rindex]["profile_image"] = profile_image;
              
            } else {
              this.noSQLObj.push({ name, email, phone_number, profile_image });
            }
          }
          
  
          this.storage.set('user', JSON.stringify(this.noSQLObj));
          resolve(this.noSQLObj)
        })
        
      })
    }


  }

  setLogin(phone_number, token, fcm_token) {

    if (this.isDevice()) {
      return new Promise((resolve) => {
        this.database.executeSql("UPDATE users SET active=? where active=?", [0, 1]).then((res1) => {
          this.database.executeSql("UPDATE users SET token=?, fcm_token=?, active=? where phone_number=?", [token, fcm_token, 1, phone_number]).then((res2) => {
            resolve(true);
          }).catch(err => resolve(false));
        }).catch(err => resolve(false))
      })
    } else {
      return new Promise(resolve => {
        this.storage.get('user').then( v => {
          this.noSQLObj = JSON.parse(v);
          for (var i = 0; i < this.noSQLObj.length; i++) {
            this.noSQLObj[i]['active'] = 0;
          }

          const rindex = this.noSQLObj.findIndex(x => x.phone_number == phone_number);

          if (rindex != -1) {
            this.noSQLObj[rindex]["phone_number"] = phone_number;
            this.noSQLObj[rindex]["token"] = token;
            this.noSQLObj[rindex]["fcm_token"] = fcm_token;
            this.noSQLObj[rindex]['active'] = 1;
            this.storage.set('user', JSON.stringify(this.noSQLObj));
            resolve(true)
          } else {
            resolve(false)
          }
        })
        


      })
    }

  }

  getCurrentUserAuthorizationToken() {
    if (this.isDevice()) {
      return new Promise(resolve => {
        // this.getDb().then( db => {
        this.database.executeSql("SELECT token FROM users where active=?", [1]).then(d => {
          const data = this.getRows(d);
          if (data.length > 0) {
            resolve(data[0]['token']);
          } else {
            resolve(null);
          }

        }).catch(err => { console.error("ENONET", err); resolve(null) })
      })
    } else {
      return new Promise(resolve => {
        this.storage.get('user').then( v => {
          if(!v){
            resolve(null);
          }else{
            this.noSQLObj = JSON.parse(v);
            const rindex = this.noSQLObj.findIndex(x => x.active == 1);

            if (rindex != -1) {
              this.storage.set('user', JSON.stringify(this.noSQLObj));
              resolve(this.noSQLObj[rindex]['token']);
            } else {
              resolve(null)
            }
          } 
          
        })
        
      })


    }

  }

  setLogout() {
    if (this.isDevice()) {
      return new Promise(resolve => {
        this.database.executeSql("UPDATE users SET token=?, fcm_token=?, active=?  where active=?", [null, null, 0, 1]).then(() => { resolve() }, err => { console.error(err); resolve() })
      })
    }else{
      return new Promise(resolve => {
        this.storage.get('user').then( v => {
          this.noSQLObj = JSON.parse(v);
          const rindex = this.noSQLObj.findIndex(x => x.active == 1);

          if (rindex != -1) {
            this.noSQLObj[rindex]['active'] = 0;
            this.storage.set('user', JSON.stringify(this.noSQLObj));
            resolve();
          } else {
            resolve();
          }
        })
        
        
      })
    }
    
  }

  setFcmToken(fcm_token) {

    if (this.isDevice()) {
      return new Promise((resolve) => {
        this.database.executeSql("UPDATE users SET fcm_token=? where fcm_token IS NOT NULL", [null]).then((res1) => {
          this.database.executeSql("UPDATE users SET fcm_token=? where active=?", [fcm_token, 1]).then((res2) => {
            resolve(true);
          }).catch(err => resolve(false));
        }).catch(err => resolve(false))
      })
    }else{
      return new Promise((resolve) => {
        this.storage.get('user').then( v => {
          this.noSQLObj = JSON.parse(v);
          for (var i = 0; i < this.noSQLObj.length; i++) {
            this.noSQLObj[i]['fcm_token'] = null;
          }

          const rindex = this.noSQLObj.findIndex(x => x.active == 1);

          if (rindex != -1) {
            this.noSQLObj[rindex]['fcm_token'] = fcm_token;
            this.storage.set('user', JSON.stringify(this.noSQLObj));
            resolve(true);
          } else {
            resolve(false);
          }
        })
        
      })
    }
    
  }

  setCanBeResident(int) {

    if (this.isDevice()) {
      return new Promise((resolve) => {
        this.database.executeSql("UPDATE users SET can_be_resident=? where active=?", [int, 1]).then((res2) => {
          resolve(true);
        }).catch(err => resolve(false));
      })
    } else {
      return new Promise((resolve) => {
        this.storage.get('user').then( v => {
          this.noSQLObj = JSON.parse(v);
          const rindex = this.noSQLObj.findIndex(x => x.active == 1);

          if (rindex != -1) {
            this.noSQLObj[rindex]['can_be_resident'] = int;
            this.storage.set('user', JSON.stringify(this.noSQLObj));
            resolve(true);
          } else {
            resolve(false);
          }
        })
        
      })
    }
    
  }

  switchLogin(phone_number) {

    if (this.isDevice()) {
      return new Promise((resolve) => {
        this.database.executeSql("UPDATE users SET fcm_token=?, active=? where active=?", [null, 0, 1]).then((res1) => {
          this.database.executeSql("UPDATE users SET active=? where phone_number=?", [1, phone_number]).then((res2) => {
            resolve(true);
          }).catch(err => { console.error(err); resolve(false) });
        }).catch(err => { console.error(err); resolve(false) });
      })
    } else {
      return new Promise((resolve) => {
        this.storage.get('user').then( v => {
          this.noSQLObj = JSON.parse(v);
          const rindex1 = this.noSQLObj.findIndex(x => x.active == 1);
          if( rindex1 != -1){
            this.noSQLObj[rindex1]['fcm_token'] = null;
            this.noSQLObj[rindex1]['active'] = 0;
          }
    
          const rindex = this.noSQLObj.findIndex(x => x.phone_number == phone_number);
    
          if (rindex != -1) {
            this.noSQLObj[rindex]['active'] = 1;
            this.storage.set('user', JSON.stringify(this.noSQLObj));
            resolve(true);
          } else {
            resolve(false);
          }
        })
        
      })

      
    }
    
  }

  private getRows(data) {
    var items = []
    for (let i = 0; i < data.rows.length; i++) {
      let item = data.rows.item(i);

      items.push(item);
    }
    // console.log("RITEMs", items);
    return items;
  }
}
