import { Injectable } from '@angular/core';
import { Platform, ToastController, AlertController, LoadingController, PickerController, ModalController } from '@ionic/angular';
import { FormGroup } from '@angular/forms';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { Calendar } from '@ionic-native/calendar/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { PickerOptions } from '@ionic/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';

declare var google;

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  loading: any;

  constructor(
    private toastCtrl: ToastController,
    public atrCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private iab: InAppBrowser,
    private callNumber: CallNumber,
    private calendar: Calendar,
    private diagnostic: Diagnostic,
    private openNativeSettings: OpenNativeSettings,
    private pickerCtrl: PickerController,
    private camera: Camera,
    private launchNavigator: LaunchNavigator
  ) { }

  async showLoader() {

    if (this.loading == null) {
      this.loading = await this.loadingCtrl.create();
      await this.loading.present();
    }

  }

  async hideLoader() {

    if (this.loading != null) {
      await this.loading.dismiss();
      this.loading = null;
    }

  }

  async promptAlert(heading, msg, okText = 'Agree', cancelText = 'Disagree' ){
    return new Promise( async resolve => {
      const alert = await this.atrCtrl.create({
        header: heading,
        message: msg,
        buttons: [
          {
            text: cancelText,
            role: 'cancel',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
              resolve(null);
            }
          }, {
            text: okText,
            handler: () => {
              console.log('Confirm Okay');
              resolve(true);
            }
          }
        ]
      });
  
      await alert.present();
    });
  }

  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom'
    });

    toast.present();
  }

  async presentSuccessToast(msg) {

    const toast = await this.toastCtrl.create({
      message: this.capitalizeEachFirst(msg),
      duration: 5000,
      position: 'top',
      cssClass: 'successToast'
    });

    toast.present();
  }

  async presentFailureToast(msg) {
    const toast = await this.toastCtrl.create({
      message: this.capitalizeEachFirst((msg) ? msg : 'ERROR'),
      duration: 5000,
      position: 'top',
      cssClass: 'failureToast'
    });

    toast.present();
  }

  async showAlert(msg) {
    return new Promise(async resolve => {
      const alert = await this.atrCtrl.create({
        header: 'Alert !',
        message: msg,
        buttons: ['OK']
      });
      alert.onDidDismiss().then( () => { resolve(); }) ;
      alert.present();
    });
  }

  async presentPickerOptions(name, options, okText = 'Done', cancelText = 'Cancel'){
    return new Promise( async resolve => {
      
      const opts: PickerOptions = {
        buttons: [
          {
            text: cancelText,
            role: 'cancel'
          },
          {
            text: okText
          }
        ],
        columns: [{name,options}]
      };
      const picker = await this.pickerCtrl.create(opts);
      picker.present();
      picker.onDidDismiss().then(async data => {
        const col = await picker.getColumn(name);
        const v = col.options[col.selectedIndex].value;
        resolve(v);


      });
    });
  }

  capitalizeEachFirst(str) {
    const splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
      // You do not need to check if i is larger than splitStr length, as your for does that for you
      // Assign it back to the array
      splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    // Directly return the joined string
    return splitStr.join(' ');
  }

  checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      const passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({ notEquivalent: true });
      }
      else {
        return passwordConfirmationInput.setErrors(null);
      }
    };
  }

  getOnlyDigits(phoneNumber) {
    return ('' + phoneNumber).replace(/\D/g, '');
  }

  onkeyupFormatPhoneNumberRuntime(phoneNumber) {
    if (phoneNumber == null || phoneNumber == '') { return phoneNumber; }

    phoneNumber = this.getOnlyDigits(phoneNumber);
    phoneNumber = phoneNumber.substring(0, 10);

    var cleaned = ('' + phoneNumber).replace(/\D/g, '');

    function numDigits(x) {
      return Math.log(x) * Math.LOG10E + 1 | 0;
    }

    // only keep number and +
    var p1 = cleaned.match(/\d+/g);
    if (p1 == null) { return cleaned; }
    var p2 = phoneNumber.match(/\d+/g).map(Number);
    var len = numDigits(p2);
    // document.write(len + " " );
    switch (len) {
      case 1:
      case 2:
        return '(' + phoneNumber;
      case 3:
        return '(' + phoneNumber + ')';
      case 4:
      case 5:
      case 6:
        var f1 = '(' + phoneNumber.toString().substring(0, 3) + ')';
        var f2 = phoneNumber.toString().substring(len, 3);
        return f1 + ' ' + f2;
      default:
        f1 = '(' + phoneNumber.toString().substring(0, 3) + ')';
        f2 = phoneNumber.toString().substring(6, 3);
        var f3 = phoneNumber.toString().substring(len + 1, 6);
        return f1 + ' ' + f2 + '-' + f3;
    }
  }

  parseName(input) {

    const capitalize = (s) => {
      if (typeof s !== 'string') { return ''; }
      s = s.toLowerCase();
      return s.charAt(0).toUpperCase() + s.slice(1);
    };

    const fullName = input || '';
    const result = {
      name: '',
      middle_name: '',
      last_name: ''
    };

    if (fullName.length > 0) {
      let nameTokens = fullName.match(/\w*/g) || [];
      nameTokens = nameTokens.filter(n => n);

      if (nameTokens.length > 3) {
        result.name = nameTokens.slice(0, 2).join(' ');
        result.name = capitalize(result.name);
      } else {
        result.name = nameTokens.slice(0, 1).join(' ');
        result.name = capitalize(result.name);
      }

      if (nameTokens.length > 2) {
        result.middle_name = nameTokens.slice(-2, -1).join(' ');
        result.last_name = nameTokens.slice(-1).join(' ');
        result.middle_name = capitalize(result.middle_name);
        result.last_name = capitalize(result.last_name);
      } else {
        if (nameTokens.length == 1) {
          result.last_name = '';
          result.middle_name = '';
        } else {
          result.last_name = nameTokens.slice(-1).join(' ');
          result.last_name = capitalize(result.last_name);
          result.middle_name = '';
        }

      }
    }

    //var display_name = result['last_name'] + (result['last_name'] ? ' ' : '') + result['name'];
    return result;
  }
  
  isLastNameExist(input) {
    var fullname = this.parseName(input);
    return !(fullname['lastName'] == '');
  }

  isPhoneNumberValid(number) {
    var _validPhoneNumber = this.getOnlyDigits(
      number
    );
    return (_validPhoneNumber.toString().length < 10) ? false : true;
  }

  openContactFormUrl() {
    var link = 'https://zuulsystems.com/contact/';
    const browser = this.iab.create(link, '_blank', 'location=no');
    browser.show();
  }

  public dialMyPhone(num) {
    this.callNumber.callNumber(num, true);
  }

  stringToBoolean(string: String){
    switch(string.toLowerCase().trim()){
        case 'true': case 'yes': case '1': return true;
        case 'false': case 'no': case '0': case null: return false;
        default: return Boolean(string);
    }
  }

  parseAddressFromProfile(__profile) {
    return `${__profile['apartment'] || ''} ${__profile['street_address'] || ''} ${__profile['city'] || ''} ${__profile['state'] || ''} ${__profile['zip_code'] || ''}`;
  }

  public customMDYHMformatDateMDYHM(_date) {

    if (_date == null) { return _date; }
    // format must be 04-17-2019 08:13 // 24 hour format
    var date = _date.split(' ')[0];
    var time = _date.split(' ')[1];
    var hour = parseInt(time.split(':')[0]);
    var minutes = parseInt(time.split(':')[1]);

    var ampm = 'AM';
    if (hour > 12) {
      hour = (hour - 12);
      ampm = 'PM';
    }

    var _hour = hour.toString();
    var _minutes = minutes.toString();

    if (_hour.length < 2) _hour = '0' + _hour;
    if (_minutes.length < 2) _minutes = '0' + _minutes;





    return date + ' ' + [_hour, _minutes].join(':') + ' ' + ampm;
  }

  public formatDateTime(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear(),
      minutes = d.getMinutes(),
      hour = d.getHours();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-') + ' ' + [hour, minutes].join(':');
  }

  getCoordsForGeoAddress(address, _default = true) {

    
    return new Promise((resolve, reject) => {
      
      const geocoder = new google.maps.Geocoder;
      geocoder.geocode({ address }, function (results, status) {
        if (status === 'OK') {
          if (results[0]) {


            var loc = results[0].geometry.location;
            var lat = loc.lat();
            var lng = loc.lng();
            resolve({ lat: lat, lng: lng });

          } else {


            //this.presentToast('No results found');
            if (_default == false) {
              reject('No results found');
            } else {
              this.getCoordsViaHTML5Navigator().then(coords => {
                resolve(coords);
              }, err => {
                reject(err);
              });
            }


          }
        } else {
          reject({results, status});
        }
      });
    });

  }

  getCoordsViaHTML5Navigator() {

    return new Promise((resolve) => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          resolve(pos);

        }, function () {
          resolve({ lat: 51.5074, lng: 0.1278 });
        });
      } else {
        // Browser doesn't support Geolocation
        resolve({ lat: 51.5074, lng: 0.1278 });
      }
    });


  }

  addaremondertodate(item) {

    var options = this.calendar.getCalendarOptions();
    options['allday'] = false;
    var self = this;

    this.diagnostic.isCalendarAuthorized().then(status => {

      if (status == false) {
        this.diagnostic.requestCalendarAuthorization().then(async response => {
          console.log(response);
          if (response == 'denied' || response == 'denied_always' ) {
            const flag = await self.promptAlert('Permission Denied', 'Do you want to allow permissions to access calender on this device?' );
            if(flag){
              this.openNativeSettings.open('application_details');
            }
          }

        }, err => {});
      }

      if (status == true) {
        this.calendar.createEventInteractivelyWithOptions(item.event_name, item.description, item.notes, new Date(item.startDate), new Date(item.endDate), options);
      }
    });



  }

  async snapImage(type) {

    return new Promise( async resolve => {
      const opts: PickerOptions = {
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel'
          },
          {
            text: 'Done'
          }
        ],
        columns: [
          {
            name: 'Upload',
            options: [
              { text: 'Camera', value: '1' },
              { text: 'Gallery', value: '0' }
            ]
          }
        ]
      };
      const picker = await this.pickerCtrl.create(opts);
      picker.present();
      picker.onDidDismiss().then(async data => {
        const col = await picker.getColumn('Upload');
        const v = col.options[col.selectedIndex].value;
        if (v == null) { resolve(null); }
        const options: CameraOptions = {
          quality: 100,
          targetWidth: 1024,
          saveToPhotoAlbum: false,
          destinationType: this.camera.DestinationType.DATA_URL,
          encodingType: this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE,
          sourceType: parseInt(v)
        };

        this.camera.getPicture(options).then((imageData) => {
          resolve(imageData);
        });

      });

    });
  }





  combineFullName(firstname, middlename, lastname){
    return `${firstname ? firstname : ''}${middlename ? ' ' + middlename : ''}${lastname ? ' ' + lastname : ''}`;
  }

  seperateFullName(input){
    const capitalize = (s) => {
      if (typeof s !== 'string') { return ''; }
      s = s.toLowerCase();
      return s.charAt(0).toUpperCase() + s.slice(1);
    };

    const fullName = input || '';
    const result = {
      first_name: '',
      middle_name: '',
      last_name: ''
    };

    if (fullName.length > 0) {
      var nameTokens = fullName.match(/\w*/g) || [];
      nameTokens = nameTokens.filter(n => n);

      if (nameTokens.length > 3) {
        result.first_name = nameTokens.slice(0, 2).join(' ');
        result.first_name = capitalize(result.first_name);
      } else {
        result.first_name = nameTokens.slice(0, 1).join(' ');
        result.first_name = capitalize(result.first_name);
      }

      if (nameTokens.length > 2) {
        result.middle_name = nameTokens.slice(-2, -1).join(' ');
        result.last_name = nameTokens.slice(-1).join(' ');
        result.middle_name = capitalize(result.middle_name);
        result.last_name = capitalize(result.last_name);
      } else {
        if (nameTokens.length == 1) {
          result.last_name = '';
          result.middle_name = '';
        } else {
          result.last_name = nameTokens.slice(-1).join(' ');
          result.last_name = capitalize(result.last_name);
          result.middle_name = '';
        }

      }
    }
    
    return result;
  }

  public async getCurrentLocation(lat, long) {

    var self = this;
    const loc = await this.getCoordsViaHTML5Navigator();
    var lt = (loc['lat']) ? loc['lat'] : 0;
    var lg = (loc['lng']) ? loc['lng'] : 0;
  // var coords = {lat: lt, lng: lg};
    this.launchNavigator.availableApps()
    .then(data => {

      if (data['google_maps']) {
        var options: LaunchNavigatorOptions = {
          start: [lt, lg],
          app: this.launchNavigator.APP.GOOGLE_MAPS
        };
        this.launchNavigator.navigate([lat, long], options);
      } else if (data['apple_maps']) {
        options = {
          start: [lt, lg],
          app: this.launchNavigator.APP.APPLE_MAPS
        };
        this.launchNavigator.navigate([lat, long], options);

      } else {
        self.presentToast('No Map software available');
      }
    });

    // geocoder.get('current_location').then((loc) => {

    

    //     })
    //     .catch(err => {
    //       self.presentToast(err);
    //     });


    // });


  }

  formatHoursToText(hour) {

    var lbl = '';
    switch (hour.toString()) {
      case '3':
        lbl = '3 Hours';
        break;
      case '6':
        lbl = '6 Hours';
        break;
      case '12':
        lbl = '12 Hours';
        break;
      case '24':
        lbl = '24 Hours';
        break;
      case '48':
        lbl = '48 Hours';
        break;
      case '168':
        lbl = '1 Week';
        break;
      case '336':
        lbl = '2 Weeks';
        break;
      case '720':
        lbl = '1 Month';
        break;
      case '876000':
        lbl = 'No Limit';
        break;
      default:
        lbl = hour + ' Hours';
        break;

    }

    return lbl;

  }


  

}
