import { NetworkService } from './network.service';
import { UtilityService } from './utility.service';
import { Injectable } from '@angular/core';
import { SqliteService } from './sqlite.service';
import { Events, Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { FcmService } from './fcm.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  User: any;
  isEmailVerificationPending = false;
  canBeResident = true;
  canShowSettings = true;
  avatar =
    'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAMAAACahl6sAAAAM1BMVEUKME7///+El6bw8vQZPVlHZHpmfpHCy9Ojsbzg5ekpSmTR2N44V29XcYayvsd2i5yTpLFbvRYnAAAJcklEQVR4nO2d17arOgxFs+kkofz/154Qmg0uKsuQccddT/vhnOCJLclFMo+//4gedzcApf9B4srrusk+GsqPpj+ypq7zVE9LAdLWWVU+Hx69y2FMwAMGyfusLHwIpooyw9IAQfK+8naDp3OGHvZ0FMhrfPMgVnVjC2kABOQ1MLvi0DEIFj1ILu0LU2WjNRgtSF3pKb4qqtd9IHmjGlJHlc09IHlGcrQcPeUjTAySAGNSkQlRhCCJMGaUC0HSYUx6SmxFAtJDTdylsr4ApC1TY0yquKbCBkk7qnYVzPHFBHkBojhVJWviwgPJrsP4qBgTgbQXdsesjm4pDJDmIuswVZDdFx0ENTtkihoeqSDXD6tVxOFFBHndMKxWvUnzexpIcx/Gg2goJJDhVo6PCMGRAnKTmZuKm3wcJO/upphUqUHy29yVrRhJDORXOKIkEZDf4YiRhEF+iSNCEgb5KY4wSRDkB/yurUEG8nMcocgYABnvbrVL3nMIP0h/d5udKnwzSC/InfPdkJ6eWb0PJE++dyVVyQP5iQmWW27X5QG5druEKafBu0Hqu9saVOHa8HKC/K6BzHKZiRMEZCDF0Nd1/ZfXI/fcOibHOssFgokg9uFA20BhztHEAZIjIohrD/o1wljeFBDEwBo8YUt5Ir/rNLjOIACPFdy/AbEcPdcJBOCxytjeYAM4Kzp6rhOIPhRGNzwmFP3rOoTFI0irtnQKx6fj1Zt+h9njEUS9mKJxfFRrX5lt7wcQtaWTOfTHeIXVJQcQrRW+OYex2j0a66XZINoO8a7fPH2iHF2mC7ZBtB3Czb5QvjizSx7A3308mRzqAwujSywQbYfwc0iU8zqjS0yQ6ztEHX9332KCaGNIYB/Qq1z3yN0oDZBWyeFYJBCkm2sXLhDtpKFwNDMu5TnrZpYGiHbK4Nlwikg5DrYV1g6iPoJmzE5MKd/fOp53EPUaQZaLqH3u+vo2ELWp3wSyWuYGoj9EEIJoV3L9AUS/ZLsJpLNBXmqOu0CW6P5A/dx9IL0FAji/FYKot9EqE0Tvs6QBUe/2CxMEkZAlBNGPhdoAQWyTSmbxUwvUygwQyMmniAPgLt87CODXHuftWJIQgzrfQDC5AfwSgz9MmmG/gWCOqDgZ4JsQeTvZBoJJDhAFEsSDyxUEEUUekk0UEMhjBcEcGsoWVpBU3NcCgkkPkJWrKbdRZvULCMTWhYEdMrayBQRyqHcnSLmAIH7LcWJ8Hch7BsHEdWFpJsZjziCgFBpZ9TPm4e0XBJTTJKt9xjy8RoLI4gimPLP5goCSgWTrEcyzsy8IqmZVMo0H5bJiQToBCOjZ5RcElhjLN3dU7uQMAvoxwQkJZKI1CQzCthJYEigahHuDDi4rFwzCPQ7F1fiDQZgTR5iJwEGYRgIsiECD8BwwMAEfDcIaW8CRBQdhjS1kJQEchDEFhiRKr4KDFPS9FGQNVwEHoW83QjsEHdkfnuIOl6C1NjMItiaCaCWgbdpFJXQ9soh2uoB9aJcCxFdgZwlcrTmvENGlrITBBdpK25Qhd1F2RScq8CKu/gsCL8qN5THjy+Rr5E6joYgPxpdl518QrCf8Kpgjn6C8HLkbb+vt7ZM8wdVvy258khsRfHaS5DalDnlidZT7Erk+SXV5Bj1D3LS29XyhVJuoKHs9Q8S6reK11oUc7vPcr9uswP3SLiDINefXOF5rwCuGzVT6zVkVPfh2wWmHcz4wAwba2cgN1/Tsvleu7//i69CgVyt1GwjOs2+XK3rtbl151Tg3vOeioG40Mz2V+6pQ4xbJHOZj6g0EMxk93tV7fuedvVZpQSPhbwNBGInrymGrwNh1GXmL8F+lAaJ+NU/fzcmvJqvKj7177+1v1GY/GiBKI1Fdy/2XK6upXwaIJpI8B/399W0mH9zzafKaeCF9J0WF+jyCuFusTGzZKhFH8dVLZql2brxgcdVBKb7KG/7UZTmB3XJ6uL/QYT5ScRI74FcHEJ7feopyfGkaeaGlPoCw/BbjZmSBWIvINQNmTxdjWJqwUI8sztR4nYPuIPSTSUnOCZOE3ierqRoJfNSQxDjLEYs8i91eqgFCDSWiFHiuqAN9CwEGCPEISVjvwhS7Mfx6dtX8kC5aqvneGBOEFN2v6RBiYwr3DQOkLhEW6fHFbIwFQnkLiWYmZxE220z/aedPx99C+hiyKR4OzNFhg8S75CJTnxQ1dyugHTLaY10iu9dBpmhQtMz1ABLrkgtHVnRsPUO3OcU25i8cWdGxZbflCBKJqBdMs3aF/dYhNexU9RFcYEmLXYQKghyWdufyldBSU3KpjkKhZclxTXQGCTkL/HZDUIH5+Gkt4SgoCtj7pSYSNJLTK3VVRnmXZxebSMBIzmHABeIdXBebiN9eHYtUZ62ab3BdGkUm+SKJw1bdRXeewaX7qqdAnljg2sVxg3guAk3baofcg9yZ2eZpnHNvSFrEqhB9YPjesmt0pt6Xc8hl7W5L9Q4Xx09ctsrd5VhWeF6nF8SRrZdw49qns//0xTK/AZ8vGr3caTliuzeFNeCJTgafpKlhHd2WP1sy1LqDF798gjKJPLqDr9keoTd43+NyNzC1CI8Xy2lcPtOaVBI5IiAWyQ3e125AcKoXs2Djhy5eVc3KiBxREIPkhjBiLhIjU++4T91IbggjRiCJLSEIwWGddkEaxlVN5KCArPHk8mXVpHk8FHH7JL3n5dPA7C90q7XkeFJucacNmGXeRfswLE71HA79efaGiCN/Ofjmfmtcp8X10tIsqCacV5xfRWjNUiXGYbovWgyFYHcQLak15K9oM5zqmgaeKsHJetbSHfSPzXOiw/rxE9YH4CXaUpsZ0ztemFurP95Jpyvrd29YTpIZr7cEJHqfc7Wl0PFm2+yJR70udaokKFtGPTdm8WdQe24+HmVLlueboWQquBcYYVH2vEzfh8kCks1p90eWsLCyZ8qK7E86Oe+3XYFnBuiWdth20UqZR5SvMoyPg3WNauJipi0LMTQgVq5xUUlZcrPsopPHJ926z8pm7xyFLrH/PxpHSoXKdWgXsLn1scZn1ZDd/2vszN3lt254qkE+qu3yoqLM+ghN3Qz2qcVzUC/ZMFsK/alU6l0OWV/bQz6v6yYbyuN5BaZ4A7Y30vs/PPksS2+qzlvfF7OQmzzcL7W+xa7OIfRuVdtn/tdvdFLnL4OTKcm2W16PmWc4FWWXNSlWM2n3D+uPxuyrcfo74aP+Ac30a82+oLmfAAAAAElFTkSuQmCC';

  rolekeys = {
    super_admin: 'Super Administrator / Owner',
    sub_admin: 'Sub-Administrators (Community Admins)',
    employees: 'Employees',
    family_head: 'Head of the Family',
    family_member: 'Family Member',
    guests_outsiders_one_time: 'Guests / Outsiders One Time',
    guests_outsiders_daily: 'Guests / Outsiders Daily'
  };

  constructor(
    public sqlite: SqliteService,
    public events: Events,
    public network: NetworkService,
    public utility: UtilityService,
    public router: Router,
    public fcmService: FcmService,
    public platform: Platform
  ) {
    this.assignEvents();
  }

  assignEvents() {
    this.events.subscribe('user:logout', this.logout.bind(this));
    this.events.subscribe('user:login', this.login.bind(this));
    this.events.subscribe('user:get', this.getUser.bind(this));
  }

  isEmailExistInSystem(email){
    return new Promise( resolve => {
      const emailObj = { email };
      this.network.checkEmailAlreadyInUse(emailObj).subscribe( res => {
        const result = res as any;
        const user_count = parseInt(result.user_count);
        if ( user_count > 0){
          // prompt user to use this email
          this.promptUserToUseEmail(email).then( res => {
            resolve(res);
          })
        }else{
          resolve(null);
        }
  
      });
    });
    
  }

  promptUserToUseEmail(email){

    return new Promise( async resolve => {
      const flag = await this.utility.promptAlert('EmailAlert', 
      email + ' is already in use, try other email or get permission to use this email address',
      'Get Permission', 'Cancel');

      if(flag){
        this.sendVerifyEmailAddress(email).then( res => {
          resolve(res);
        });
      }

    })
  }

  sendVerifyEmailAddress(email){

    return new Promise( resolve => {
      const emailObj = { email }
      console.log(emailObj);

      this.network.verifyEmailAddress(emailObj).subscribe( res => {
        this.utility.presentSuccessToast(res['message']);
        resolve(true);
      }, err => { resolve(false) })

    })


  }

  login(user) {

    var email_data = {
      email: user.email,
      password: user.password,
    }

    var phone_data = {
      phone_number: user.phone_number,
      password: user.password,
      register_with_phonenumber: true
    }

    var data = (user.register_with_phonenumber == true) ? phone_data : email_data

    this.network.login(data).subscribe(async res => {


      console.log(res);

      var user = res['user'];
      const token = user['token'];
      console.log(token);
      const phone_number = user['phone_number'];

      var fcm_token = null;
      if (this.platform.is('cordova')) {
        fcm_token = await this.fcmService.getFCMToken();
      }

      var self = this;
      self.setUserInDB(user).then(() => {
        self.setLogin(phone_number, token, fcm_token).then(() => {
          self.processUserData(user).then(() => {
            this.router.navigate(['/tabs/dashboard']);
          })
        }, err => console.error(err));
      }, err => console.error(err));

    }, err => { console.error(err) })

  }

  async setTokenToServer(){
    const fcm_token = await this.fcmService.getFCMToken();
    if(fcm_token){
      this.network.saveFcmToken({token: fcm_token}).subscribe(dats => {
      }, err => {  });
    }
    
  }

  getUser() {

    return new Promise(resolve => {
      this.network.getUser().subscribe(async (user: any) => {
        console.log(user);
        if (user.success) {
          this.processUserData(user.success).then(r => resolve(r));
        } else {
          this.logout();
          resolve(null);
        }
      }, err => {
        console.error(err);
        this.logout();
        resolve(null);
      });
    })

  }

  private async processUserData(user) {

    return new Promise(resolve => {
      this.User = user;
      this.isEmailVerificationPending = (this.User.email_verification_code != null);
      if (this.User.is_guard == '1') {
        this.utility.presentToast('Guard / Security Personnal can\'t login here');
        this.logout();
        resolve(null)
      } else {
        this.canBeResident = this.canUserBecomeResident(user);
        user['canBeResident'] = this.canBeResident;
        this.setUser(this.User);
        this.canBeResident = this.canUserBecomeResident(this.User);
        this.User.canBeResident = this.canBeResident;

        // Add Settings page as required
        this.isShowSettings();
        // this.router.navigate(['/tabs/dashboard'])
        resolve(true);

      }
    })

  }



  setUser(user) {
    this.User = user;
  }

  isShowSettings() {
    const roles = this.User.roles[0];
    return (roles.id != 7) ? true : false;
  }

  setUserInDB(user) {
    const name = user.fullName;
    const email = user.email;
    const phone_number = user.phone_number;
    const profile_image = user.profileImageUrl;
    return this.sqlite.addRecord(name, email, phone_number, profile_image)
  }

  getAuthToken() {
    return this.sqlite.getCurrentUserAuthorizationToken();
  }

  setLogin(phone_number, token, fcm_token) {
    return this.sqlite.setLogin(phone_number, token, fcm_token);
  }

  getAllUsers() {
    return this.sqlite.getAllRecords();
  }

  getCurrentUser() {
    return this.User;
  }

  switchUserAccount(sw_user) {
    return new Promise(resolve => {
      if (sw_user.token == null) {
        resolve(null);
      } else {
        // token not null, try to login
        // console.log(sw_user.phone_number);
        this.sqlite.switchLogin(sw_user.phone_number).then(ret => {
          // console.log(ret);
          if (!ret) {
            resolve(null);
          } else {
            // this.events.publish('user:get');
          }
        })
      }
    })

  }



  logout() {
    this.User = null;
    this.sqlite.setLogout();
    this.router.navigate(['login']);
  }

  isUserEmailPendingVerification(user) {

    if (!user) {
      return false;
    }

    if (user.email_verification_code != null) {
      return true;
    }

    return false;

  }


  canUserBecomeResident(user) {
    var canResident = false;
    let roles = Object.keys(this.rolekeys);

    switch (user.roles[0].name) {
      case roles[0]: // super_admin
        break;
      case roles[1]: // sub_admin
        break;
      case roles[2]: // employees
        break;
      case roles[3]: // family_head
        canResident = true;
        break;
      case roles[4]: // family_member
        canResident = true;
        break;
      case roles[5]: // guests_outsiders_one_time
        break;
      case roles[6]: // guests_outsiders_daily
        canResident = true;
        break;
      default:
        break;
    }

    this.User.canBeResident = (canResident && !!user.is_verified);
    return this.User.canBeResident;
  }

  canSendPasses(user) {

    if (!user) {
      return false;
    }

    if (user.suspand == '1') {
      return false;
    }
    if (user.head_of_family == null) {
      return false;
    }

    if (user.head_of_family != null) {

      if (user.head_of_family == '1') {
        return true;
      }

      if (user.head_of_family == '0') {

        if (user.can_send_passes == null) {
          return false;
        }

        if (user.can_send_passes != null) {

          if (user.can_send_passes == '0') {
            return false
          }

          if (user.can_send_passes == '1') {
            return true;
          }

        }

      }

    }

    return false;
  }
}
