import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SqliteService } from './services/sqlite.service';
import { FcmService } from './services/fcm.service';
import { CountryflagService } from './services/countryflag.service';
import { ContactsService } from './services/contacts.service';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  constructor(
    private platform: Platform,
    private sqlite: SqliteService,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private fcmService: FcmService,
    private flags: CountryflagService,
    private contactService: ContactsService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      if (this.platform.is('cordova') ) {

        this.sqlite.createTable();
        this.fcmService.setupFMC();
        
      }

    });

  }

}
